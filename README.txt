
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Author:
 * Edgar Saumell

This module implements the QueryLoader solution from http://www.gayadesign.com/diy/queryloader-preload-your-website-in-style/
A jQuery script that preloads all the images included in the web page (even the images in CSS).
Demo: http://www.gayadesign.com/scripts/queryLoader/

INSTALLATION
------------

1. Put the jquery_preloader directory to your sites/all/modules directory.

2. Get the files at http://www.gayadesign.com/diy/queryloader-preload-your-website-in-style/

http://www.gayadesign.com/scripts/queryLoader/queryLoader.zip
http://www.gayadesign.com/scripts/queryLoader2/qL2.zip

3. Unzip and put inside the jQuery Preloader module folder (jquery_preloader) inside your sites/all/modules directory.

The resulting paths should be:
sites/all/modules/jquery_preloader/queryLoader/
sites/all/modules/jquery_preloader/qL2/

4. Enable the module at Administer -> Site building -> Modules.

5. Configure permissions at Administer -> User management -> Permissions.

6. Go to Administer -> Site configuration -> jQuery Preloader settings to enable and configure the settings.