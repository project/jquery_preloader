<?php
/**
 * @file
 * Provides the administration page for jQuery Preloader module.
 */

/**
 * Administration settings page
 */
function jquery_preloader_admin() {
  $form = array();

  $form['jquery_preloader'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery Preloader'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 0,
  );
  $form['jquery_preloader']['jquery_preloader_enable'] = array(
    '#title' => t('Enable jQuery Preloader'),
    '#type' => 'checkbox',
    '#weight' => 0,
    '#default_value' => variable_get('jquery_preloader_enable', 0),
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#description' => t('Credit to <a href="http://www.gayadesign.com/diy/queryloader-preload-your-website-in-style/">Gaya Design</a> for the original script.')
  );
  $form['jquery_preloader']['jquery_preloader_counter'] = array(
    '#title' => t('Enable percentage counter'),
    '#type' => 'checkbox',
    '#weight' => 0,
    '#default_value' => variable_get('jquery_preloader_counter', 0),
    '#options' => array(
      0 => t('Off'),
      1 => t('On'),
    ),
    '#description' => t('Credit to Olivier Dumetz for percentage counter.')
  );
  $form['jquery_preloader']['jquery_preloader_selector'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery Preloader selector'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t("You can setup a jQuery selector to only preload what you want."),
    '#weight' => 1,
  );
  $form['jquery_preloader']['jquery_preloader_selector']['jquery_preloader_id'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS ID selector'),
    '#default_value' => variable_get('jquery_preloader_id', ''),
    '#size' => 20,
    '#description' => t('Use an ID selector like #main, #wrapper or #header depending on the theme you are using.<br /><strong>Be aware that you can only preload a single element, so using a class selector could cause some problems!</strong>'),
    '#weight' => 0,
  );
  $form['jquery_preloader']['paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Allowed paths (NOT IMPLEMENTED YET!)'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 9,
  );
  $form['jquery_preloader']['paths']['jquery_preloader_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Use jQuery Preloader on the following pages.'),
    '#default_value' => variable_get('jquery_preloader_path', ''),
  );
  $form['jquery_preloader']['paths']['jquery_preloader_path_disallow'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not use jQuery Preloader on the following pages.'),
    '#description' => t('If a path appears here, the jQuery Preloader is not shown even if all above options apply.'),
    '#default_value' => variable_get('jquery_preloader_path_disallow', 'admin/*'),
  );

return system_settings_form($form);
} // function jquery_preloader_admin
